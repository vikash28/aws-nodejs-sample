/*
 * Copyright 2013. Amazon Web Services, Inc. All Rights Reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

// Load the SDK and UUID
var AWS = require('aws-sdk');
//var uuid = require('node-uuid');

AWS.config.loadFromPath('./AwsConfig.json');

// Create an S3 client
var s3 = new AWS.S3({apiVersion: '2006-03-01'}); 
var fs = require('fs');
var mysql = require('mysql');
var con = mysql.createConnection({
        host: "107.180.2.11",
        user: "cloudhitiadmin",
        password: "cloudhitiadmin2017",
        database: "cloudhitidemo"
});
con.connect();
// Create a bucket and upload something into it
var bucketName = 'firepie-orders';
var filelist=['deliveries.csv','order_items.csv','get_swift_deliveries.csv','orders.csv','customers.csv'];

filelist.forEach(function(item)
{
	if(item != "orders.csv") return false;
	 
	var keyName = item;
	var path1="deliveries.csv"
	var stagingTablename="firepiedeliveries"
	var delimiter=",";

	fs.readFile(keyName, function (err, data) {
    console.log("Read file from local start");
    if (err) {  console.log("Error local file"); }               
        s3.putObject({
            Bucket: bucketName,
            Key: keyName,
            Body: data
            } ,function(err, data) {  
        console.log('Successfully uploaded package in s3.' );
        console.log('Local to s3 done.');
    	}); 
		
		
		con.query("load data local infile '" + path1 + "' into table " + stagingTablename
            + " fields terminated by '" + delimiter + "' ", function (err, result, fields) {  if (err) throw err;
    		 console.log(result);
		});

	});  	
})


//console.log(new Date(year, month, day))


/*fs.readFile('hello_world.txt', function (err, data) {
    console.log("Read file from local start");
    if (err) {  console.log("Error local file"); }               
        s3.putObject({
            Bucket: bucketName,
            Key: keyName,
            Body: data
            } ,function(err, data) {  
        console.log('Successfully uploaded package in s3.' );
        console.log('Local to s3 done.');
    }); 
});  
*/

//s3.createBucket({Bucket: bucketName}, function() {
  /*var params = {Bucket: bucketName, Key: keyName, Body: 'Hello World!'};
  s3.putObject(params, function(err, data) {
    if (err)
      console.log(err)
    else
      console.log("Successfully uploaded data to " + bucketName + "/" + keyName);
  });*/
//});
